import { Injectable } from '@angular/core';
import {Producto} from "../models/producto";


@Injectable()
export class CarritoService {
  productos:Producto[] = [];
  items:number = 0;
  total:number = 0;
  hayProductos: boolean = false;
  constructor( ) {

  }

  public agregarProducto(prod:Producto, cant:number){
    prod.cantidad = cant;
    this.productos.push(prod);
    this.items += cant;
    this.total += prod.precio * cant;
    this.hayProductos = true;
  }

  public borrarCarrito(){
    this.productos = [];
    this.items = 0;
    this.total = 0;
    this.hayProductos = false;
  }

}
