
export class Producto {
  codigo: string;
  nombre: string;
  foto: string;
  descripcion: string;
  peso:string;
  fotos:Foto[];
  color:string;
  stock: string;
  dimensiones: string;
  caracteristicas: string;
  tipo_producto: string;
  precio: number;
  cantidad: number;

  constructor() {
    // Temporalmente
    this.nombre = "Nombre del Producto";
    //this.foto = "assets/img/4c9veypxRZ2mHj0pehqQ_post-inner-2.jpg";
    this.foto = "assets/img/qFi5P4IuTJRQCrRvKCrJ_post-inner-1.jpg";
    this.precio = 100000;

  }



}

class Foto{
  $key: string;
  file:File;
  name:string;
  url:string;
  progress:number;
  uploaded:boolean;
  createdAt: Date = new Date();
  constructor(file:File) {
    this.file = file;
  }
}

