import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {CarritoService} from "../../services/carrito.service";
import {NgForm} from "@angular/forms";
import {ProductosPage} from "../productos/productos";
import {MiCarritoPage} from "../mi-carrito/mi-carrito";

@Component({
  selector: 'page-mi-perfil',
  templateUrl: 'mi-perfil.html'
})
export class MiPerfilPage {
  usuario = "Nombre del usuario";
  constructor(public navCtrl: NavController, public carritoSvc: CarritoService) {
  }

  guardarPerfil(formulario: NgForm){
    alert("Datos Guardados! (de gua' u)");
    this.navCtrl.setRoot(ProductosPage);
  }

  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }
}
