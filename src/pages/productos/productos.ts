import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MiCarritoPage } from '../mi-carrito/mi-carrito';
import {CarritoService} from "../../services/carrito.service";
import {Producto} from "../../models/producto";

@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html',
})
export class ProductosPage {
  usuario = "Nombre del usuario";
  prod = new Producto();
  productos: Producto[] = [this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod];

  constructor(public navCtrl: NavController, public carritoSvc: CarritoService) {

  }
  agregarAMiCarrito(prod:Producto){
    this.carritoSvc.agregarProducto(prod,5);
  }

  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }
}


