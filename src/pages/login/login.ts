import { Component , ViewChild} from '@angular/core';
import { NavController, IonicPage, NavParams} from 'ionic-angular';
import {ProductosPage} from "../productos/productos";
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  facebook = {
    loggedIn : false,
    name : '',
    email : '',
    profilePicture: ''
  };

  @ViewChild('player') player;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private afauth:AngularFireAuth) {
  }

  ionViewWillLeave() {
    // the .nativeElement property of the ViewChild is the reference to the tag <video>
    this.player.nativeElement.src = '';
    this.player.nativeElement.load();
  }
  ionViewWillEnter() {
    this.player.nativeElement.src = 'assets/video/market_online.mp4';
    this.player.nativeElement.load();
  }

  ionViewDidLoad() {
    console.log('Hello LoginBackgroundVideo Page');
  }

  goToSignup() {
    console.log('Signup clicked');
  }

  goToLogin() {
    console.log('Login clicked');
  }


  loginwithfb() {
    this.afauth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => {
        this.facebook.loggedIn = true;
        this.facebook.email = res.user.email;
        this.facebook.name = res.user.displayName;
        this.facebook.profilePicture = res.user.photoURL;
        this.navCtrl.setRoot(ProductosPage);
      })
  }

  logoutwithfb() {
    this.facebook.loggedIn = false;
    this.afauth.auth.signOut();
  }

  goToProductos(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ProductosPage);
  }

}
