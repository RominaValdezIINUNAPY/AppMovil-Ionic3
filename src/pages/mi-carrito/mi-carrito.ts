import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {CarritoService} from "../../services/carrito.service";
import {ProductosPage} from "../productos/productos";

@Component({
  selector: 'page-mi-carrito',
  templateUrl: 'mi-carrito.html'
})

export class MiCarritoPage {
  usuario = "Nombre del usuario";
  constructor(public navCtrl: NavController, public carritoSvc: CarritoService) {
  }

  guardarCompra(){
    this.carritoSvc.borrarCarrito();
    alert("Compra Realizada! (de gua' u)");
    this.navCtrl.setRoot(ProductosPage);
  }

  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }

}

